local S    = minetest.get_translator(minetest.get_current_modname())
-- steal translated strings from other mods that are known to have them
local ST   = minetest.get_translator("mcl_throwing")
local STNT = minetest.get_translator("mcl_tnt")


-- from mcl_throwing
-- =================
-- original function by Wuzzy (MineClone2:9e31c2b62b)
-- improved by AFCMS and contributors
local function check_object_hit(self, pos, dmg)
	for _,object in pairs(minetest.get_objects_inside_radius(pos, 1.5)) do
		local entity = object:get_luaentity()
		if entity and entity.name ~= self.object:get_luaentity().name then
			if object:is_player() and self._thrower ~= object:get_player_name() then
				self.object:remove()
				return true
			elseif (entity.is_mob == true or entity._hittable_by_projectile) and (self._thrower ~= object) then
				object:punch(self.object, 1.0, {
					full_punch_interval = 1.0,
					damage_groups = dmg,
				}, nil)
				return true
			end
		end
	end
	return false
end


-- constants
-- =========
-- RANGE is the radius of TNT stick explosion
-- STACK_MAX is how many TNT sticks can be in one stack
-- QUANTITY is how many TNT sticks you get from the recipe
local RANGE = 3
local STACK_MAX = 16
local QUANTITY = 1

-- definition tables
-- =================
-- ["name"] = {
-- 	desc = S("Readable Name"),
-- 	help_special = S("Tooltip"),
-- 	param = {see mcl_explosions/API.md; this is the `info` parameter},
--	item = "mcl_additional:ingredient", -- not needed if it's "canonical"
--	timed = false, -- optional (false if omitted)
-- },
local defs = {
	["tnt_stick"] = {
		desc = S("TNT Stick"),
		help_special = S("Explodes on contact with ground"),
		param = {},
	},
	["sticky_tnt_stick"] = {
		desc = S("Sticky TNT Stick"),
		help_special = S("Explodes on contact with ground after 4 seconds"),
		param = {},
		item = "mcl_mobitems:slimeball",
		timed = true,
	},
	["fire_tnt_stick"] = {
		desc = S("Fire TNT Stick"),
		help_special = S("Explodes on contact with ground and sets blocks around on fire"),
		param = {fire=true},
		item = "mcl_fire:fire_charge",
	},
}

local canonical = "tnt_stick"

for k,v in pairs(defs) do
	local is_canonical = k == canonical

	local name    = "vl_tnt_sticks:"..k
	local texture = "vl_tnt_sticks_"..k..".png"

	-- register the TNT stick entity that will be thrown
	local ent_def = {
		physical = false,
		timer = 0,
		textures = {texture},
		visual_size = {x=0.9, y=0.9},
		pointable = false,
		get_staticdata = mcl_throwing.get_staticdata,
		on_activate = mcl_throwing.on_activate,
		on_step = function(self, dtime)
			self.timer = self.timer + dtime
			local pos = self.object:get_pos()
			local node = minetest.get_node(pos)
			local def = minetest.registered_nodes[node.name]

			-- if we hit something (that is a node)
			if self._lastpos.x then
				-- and it is something solid
				if (def and def.walkable) or not def then
					if v.timed then -- and if the stick is sticky
						-- reset velocity and acceleration to "stop"
						self.object:set_velocity(vector.new(0, 0, 0))
						self.object:set_acceleration(vector.new(0, 0, 0))

						-- check if no timer set yet
						if self.timer == 0 then
							-- FIXME: the sound doesn't play
							minetest.sound_play("tnt_ignite", { pos = pos, gain = 0.8, max_hear_distance = 10 }, true)
							-- set the timer (similar logic to TNTs)
							self.timer = tnt.BOOMTIMER - (0.5 + math.random())
						-- check if the timer expired
						elseif self.timer > tnt.BOOMTIMER then
							mcl_explosions.explode(pos, RANGE, v.param, self.object)
							self.object:remove()
						end
					else
						mcl_explosions.explode(pos, RANGE, v.param, self.object)
					end
					return
				end
			end

			-- if we hit something (that is an entity)
			-- this logic doesn't apply to sticky sticks because that would mean attaching them to entities
			-- ...and, as you can imagine, that's not as easy to implement
			if (not v.timed) and check_object_hit(self, pos, 0) then
				mcl_explosions.explode(pos, RANGE, v.param, self.object)
				self.object:remove()
				return
			end

			-- and finally, update the position
			self._lastpos = pos
		end,
		_thrower = nil,
		_lastpos = {},
	}
	--[[if v.timed then
		ent_def.collisionbox = {-0.4, -0.4, -0.4, 0.4, 0.4, 0.4}
	else
		ent_def.collisionbox = {0,0,0,0,0,0}
	end]]
	minetest.register_entity(name.."_entity", ent_def)

	-- register craftitem that players will actually interact with
	minetest.register_craftitem(name, {
		description = v.desc,
		-- this is where we use our stolen translations >:)
		_tt_help = ST("Throwable").."\n"..
			v.help_special.."\n"..
			STNT("Explosion radius: @1", tostring(RANGE)),
		_doc_items_usagehelp = ST("Use the punch key to throw."),
		inventory_image = texture,
		stack_max = STACK_MAX,
		groups = {weapon_ranged = 1},
		on_use = mcl_throwing.get_player_throw_function("vl_tnt_sticks:"..k.."_entity"),
		_on_dispense = mcl_throwing.dispense_function,
	})

	-- 22 is the velocity that every throwable object uses
	-- but these are really light, e.g. snowballs and eggs
	-- thus it is set 2 points higher here to account for "gravitation"
	mcl_throwing.register_throwable_object(name, name.."_entity", 24)

	-- construct the crafting recipe
	if not is_canonical then
		minetest.register_craft({
			type = "shapeless",
			output = name,
			recipe = {v.item, "vl_tnt_sticks:"..canonical},
		})
	end

	-- mark timed sticks as WIP
	-- (their behaviour is quite chaotic at times)
	if v.timed then mcl_wip.register_wip_item(name) end
end

minetest.register_craft({
	output = "vl_tnt_sticks:"..canonical.." "..tostring(QUANTITY),
	recipe = {
		{"mcl_mobitems:gunpowder"},
		{"group:sand"},
		{"mcl_mobitems:gunpowder"},
	},
})
